package me.deftware.aristois.installer.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import me.deftware.aristois.installer.InstallerAPI;
import me.deftware.aristois.installer.Main;
import me.deftware.aristois.installer.jsonbuilder.AbstractJsonBuilder;
import me.deftware.aristois.installer.modloader.impl.ForgeInstaller;
import me.deftware.aristois.installer.utils.Utils;
import me.deftware.aristois.installer.utils.VersionData;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * @author Deftware
 */
public class InstallerUI {

	private JButton installButton;
	private JButton cancelButton;
	private JComboBox<String> mcVersionComboBox;
	private JPanel mainPanel;
	private JLabel madeByLabel;
	private JComboBox<String> launcherComboBox;
	private JComboBox<String> modLoaderComboBox;

	public InstallerUI(JFrame frame) {
		madeByLabel.setText(madeByLabel.getText());
		frame.setTitle(String.format("Aristois %sInstaller", InstallerAPI.isDonorBuild() ? "Donor Version " : ""));
		installButton.addActionListener(e -> {
			install();
		});
		cancelButton.addActionListener((e) -> System.exit(0));
		// Populate data
		List<String> versions = new ArrayList<>();
		InstallerAPI.getVersions().keySet().forEach(k -> {
			if (k.split("\\.").length == 2) {
				k += ".0";
			}
			versions.add(k);
		});
		versions.sort(Comparator.comparingInt(value -> Integer.parseInt(value.replaceAll("\\.", ""))));
		Collections.reverse(versions);
		versions.forEach(v -> mcVersionComboBox.addItem(v.replace(".0", "")));
		mcVersionComboBox.addActionListener(e -> {
			updateComboBoxes();
		});
		updateComboBoxes();
	}

	private void updateComboBoxes() {
		if (mcVersionComboBox.getSelectedItem() != null) {
			VersionData data = InstallerAPI.getVersions().get(mcVersionComboBox.getSelectedItem().toString());
			launcherComboBox.removeAllItems();
			data.getLaunchers().forEach(launcherComboBox::addItem);
			modLoaderComboBox.removeAllItems();
			modLoaderComboBox.addItem("None");
			data.getModLoaders().forEach(modLoaderComboBox::addItem);
		}
	}

	private void install() {
		VersionData data = InstallerAPI.getVersions().get(Objects.requireNonNull(mcVersionComboBox.getSelectedItem()).toString());
		String launcher = Objects.requireNonNull(launcherComboBox.getSelectedItem()).toString(), modLoader = Objects.requireNonNull(modLoaderComboBox.getSelectedItem()).toString();
		PathSelectionUI pathSelector = new PathSelectionUI(launcher.equalsIgnoreCase("multimc") ? "Please enter your MultiMC instance you want to add Aristois to:" : "Please enter your .minecraft directory:", Utils.getMinecraftRoot(), (p) -> {
			new Thread(() -> {
				Thread.currentThread().setName("Install thread");
				DialogUI installingDialog = new DialogUI("Aristois is being installed... Please wait...", "Installing...", false, () -> { });
				installingDialog.setAlwaysOnTop(true);
				new Thread(() -> installingDialog.setVisible(true)).start();
				String result = "";
				if (modLoader.equalsIgnoreCase("forge")) {
					result = new ForgeInstaller().install(data, p);
				} else {
					AbstractJsonBuilder builder = data.getBuilder(modLoader, launcher);
					result = builder.install(builder.build(data), data, p);
				}
				DialogUI dialog = new DialogUI(result, "Install complete", true, () -> {
					System.exit(0);
				});
				installingDialog.dispose();
				dialog.setAlwaysOnTop(true);
				dialog.setVisible(true);
			}).start();
		});
		pathSelector.setAlwaysOnTop(true);
		pathSelector.setVisible(true);
	}

	public static JFrame create() {
		JFrame frame = new JFrame("");
		InstallerUI ui = new InstallerUI(frame);
		JPanel panel = ui.mainPanel;
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		frame.setContentPane(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
		return frame;
	}

	{
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
		$$$setupUI$$$();
	}

	/**
	 * Method generated by IntelliJ IDEA GUI Designer
	 * >>> IMPORTANT!! <<<
	 * DO NOT edit this method OR call it in your code!
	 *
	 * @noinspection ALL
	 */
	private void $$$setupUI$$$() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
		installButton = new JButton();
		installButton.setEnabled(true);
		installButton.setText("Install");
		mainPanel.add(installButton, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		cancelButton = new JButton();
		cancelButton.setEnabled(true);
		cancelButton.setText("Cancel");
		mainPanel.add(cancelButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		mcVersionComboBox = new JComboBox();
		final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
		mcVersionComboBox.setModel(defaultComboBoxModel1);
		mainPanel.add(mcVersionComboBox, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label1 = new JLabel();
		label1.setText("Select Launcher and ModLoader to Install to:");
		mainPanel.add(label1, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		final JLabel label2 = new JLabel();
		label2.setText("Select which Minecraft version you want to install for:");
		mainPanel.add(label2, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		launcherComboBox = new JComboBox();
		final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
		launcherComboBox.setModel(defaultComboBoxModel2);
		mainPanel.add(launcherComboBox, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		modLoaderComboBox = new JComboBox();
		modLoaderComboBox.setEnabled(true);
		mainPanel.add(modLoaderComboBox, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
		madeByLabel = new JLabel();
		madeByLabel.setText("Made by https://deftware.me/");
		mainPanel.add(madeByLabel, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(287, 16), null, 0, false));
	}

	/**
	 * @noinspection ALL
	 */
	public JComponent $$$getRootComponent$$$() {
		return mainPanel;
	}

}
